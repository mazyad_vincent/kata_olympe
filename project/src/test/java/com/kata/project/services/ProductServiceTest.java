package com.kata.project.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.catalina.connector.Request;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import com.kata.project.commons.exceptions.RequestExceptions;
import com.kata.project.converters.ProductConverter;
import com.kata.project.models.dao.ProductDao;
import com.kata.project.models.dto.ProductDto;
import com.kata.project.repositories.ProductRepository;

/**
 * Test du service ProductServices dans le package services
 */
@SpringJUnitConfig
@SpringBootTest
public class ProductServiceTest {

    @Autowired
    private ProductService productService;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private ProductConverter productConverter;

    @InjectMocks
    private ProductService productServiceMock;

    /**
     * test fonction insertion
     */
    @Test
    public void testInsertProductDao() {
        // Insertion d'une offre
        ProductDto ProductDto = new ProductDto(null, "T-shirt noir", "Adidas", "S", 10, (float) 20.99);
        productService.post(Optional.of(ProductDto));

        // Sélection de l'offre insérée
        Optional<List<ProductDto>> retrievedProductDao = productService.selectAll();

        // Vérification que l'offre insérée est récupérée avec succès
        assertNotNull(retrievedProductDao);
        assertTrue(retrievedProductDao.isPresent());
        assertEquals(60, retrievedProductDao.get().size()); // initialement 59 données

        ProductDto lastInserted = retrievedProductDao.get().get(retrievedProductDao.get().size() - 1);
        assertEquals("T-shirt noir", lastInserted.getProductName());
        assertEquals("Adidas", lastInserted.getProductBrand());
        assertEquals("S", lastInserted.getProductSize());
        assertEquals(10, lastInserted.getQuantity());

    }

    /**
     * test de la fonction de seleciton
     */
    @Test
    public void testSelectAll() {
        // il y'a initialement 59 données
        Optional<List<ProductDto>> retrievedProductDao = productService.selectAll();
        assertEquals(true, retrievedProductDao.isPresent());
        assertEquals(59, retrievedProductDao.get().size());
        
        // on mock la selection 
        List<ProductDao> list = new ArrayList<ProductDao>();
        ProductDao product1 = new ProductDao((long) 1, "Tshirt", "Olympe", "M", 10, (float)10.0);
        list.add(product1);
        when(productRepository.findAll()).thenReturn(list);
        when(productConverter.ConvertFromDaoToDtoModel(product1)).thenReturn(new ProductDto((long) 1, "converted", "HLM", "M", 2, (float)2.0));
        Optional<List<ProductDto>> product = productServiceMock.selectAll();
        assertTrue(product.isPresent());
        assertEquals(1, product.get().size());
    }

    /**
     * test de la fonction d'update
     */
    @Test
    public void testUpdate() {
        ProductDto productDto =  new ProductDto((long) 2, "Tshirt", "Olympe", "M", 10, (float)10.0);
        Optional<ProductDto> retrievedProductDto = productService.update(Optional.of(productDto));
        // vérification que la donnée a bien été modifiée
        assertEquals(true, retrievedProductDto.isPresent());

        // on récupére l'élement qui vient tout juste d'etre modifié
        Optional<ProductDto> productmodified = productService.selectAll().get().stream().filter(u -> u.getProductId() == 2).findFirst();
        assertTrue(productmodified.isPresent());
        assertEquals(productDto.getProductName(), productmodified.get().getProductName());
        assertEquals(productDto.getProductId(), productmodified.get().getProductId());
        assertEquals(productDto.getProductSize(), productmodified.get().getProductSize());

        // produit invalide l'id ne sera pas reconnu

        ProductDto invalidProduct =  new ProductDto((long) 1000, "Tshirt", "Olympe", "M", 10, (float)10.0);
        assertThrows(RequestExceptions.Exception404.class, () -> {
            productService.update(Optional.of(invalidProduct));
        });
    }


}
