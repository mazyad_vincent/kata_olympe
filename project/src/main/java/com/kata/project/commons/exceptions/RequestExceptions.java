package com.kata.project.commons.exceptions;

/**
 * Class parent of all Request Exceptions
 */

public class RequestExceptions {

    private static final String internal_error = "Internal Error";
    private static final String notfound_error = "Element not found";
    private static final String access_error = "You don't have the access rights";
    private static final String bad_request_error  = "Bad user request";
    private static final String unauthorized_error = "Incorrect credentials";

    public static class Exception404 extends RuntimeException{

        public Exception404(String message) {
            super(message);
        }

        public Exception404() {
            super(notfound_error);
        }
    }

    public static class Exception403 extends RuntimeException {

        public Exception403(String message) {
            super(message);
        }

        public Exception403() {
            super(access_error);
        }
    }

    public static class Exception401 extends RuntimeException {

        public Exception401(String message) {
            super(message);
        }

        public Exception401() {
            super(unauthorized_error);
        }
    }

    public static class Exception400 extends RuntimeException {
        public Exception400(String message) {
            super(message);
        }

        public Exception400() {
            super(bad_request_error);
        }
    }

    public static class Exception500 extends RuntimeException {

        public Exception500(String message) {
            super(message);
        }

        public Exception500() {
            super(internal_error);
        }
    }

}
