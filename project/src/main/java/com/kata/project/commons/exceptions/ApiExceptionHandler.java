package com.kata.project.commons.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = {RequestExceptions.Exception400.class})
    public ResponseEntity<Object> handlerApiRequestException (RequestExceptions.Exception400 e){
        final var httpStatus = HttpStatus.BAD_REQUEST;
        var exception = new ApiExceptionModel(
                e.getMessage(),
                ZonedDateTime.now(ZoneId.of("Z")),
                httpStatus.value()
        );
        return new ResponseEntity<>(exception, httpStatus);
    }
    @ExceptionHandler(value = {RequestExceptions.Exception404.class})
    public ResponseEntity<Object> handlerApiRequestException (RequestExceptions.Exception404 e){
        final var httpStatus = HttpStatus.NOT_FOUND;
        var exception = new ApiExceptionModel(
                e.getMessage(),
                ZonedDateTime.now(ZoneId.of("Z")),
                httpStatus.value()
        );
        return new ResponseEntity<>(exception, httpStatus);
    }

    @ExceptionHandler(value = {RequestExceptions.Exception403.class})
    public ResponseEntity<Object> handlerApiRequestException (RequestExceptions.Exception403 e){
        final var httpStatus = HttpStatus.FORBIDDEN;
        var exception = new ApiExceptionModel(
                e.getMessage(),
                ZonedDateTime.now(ZoneId.of("Z")),
                httpStatus.value()
        );
        return new ResponseEntity<>(exception, httpStatus);
    }

    @ExceptionHandler(value = {RequestExceptions.Exception401.class})
    public ResponseEntity<Object> handlerApiRequestException (RequestExceptions.Exception401 e){
        final var httpStatus = HttpStatus.UNAUTHORIZED;
        var exception = new ApiExceptionModel(
                e.getMessage(),
                ZonedDateTime.now(ZoneId.of("Z")),
                httpStatus.value()
        );
        return new ResponseEntity<>(exception, httpStatus);
    }

    @ExceptionHandler(value = {RequestExceptions.Exception500.class})
    public ResponseEntity<Object> handlerApiRequestException (RequestExceptions.Exception500 e){
        final var httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        var exception = new ApiExceptionModel(
                e.getMessage(),
                ZonedDateTime.now(ZoneId.of("Z")),
                httpStatus.value()
        );
        return new ResponseEntity<>(exception, httpStatus);
    }
}
