package com.kata.project.commons.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.ZonedDateTime;

@Getter
@AllArgsConstructor
public class ApiExceptionModel {

    private final String message;
    private final ZonedDateTime timestamp;
    private final int httpStatus;
}
