package com.kata.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class ProjectApplication {

	public static void main(String[] args) {
		try {
			SpringApplication.run(ProjectApplication.class, args);
		} catch (Exception e) {
			log.error("L\'application n'a pas pu démarrer ", e);
		}
	}

}
