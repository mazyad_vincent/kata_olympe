package com.kata.project.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kata.project.commons.exceptions.RequestExceptions;
import com.kata.project.converters.BasicConverter;
import com.kata.project.models.dao.BasicDao;
import com.kata.project.models.dto.BasicDto;

import lombok.extern.slf4j.Slf4j;

/**
 * super class utilisée pour les Services
 * ...)
 * 
 * @param <Z> le Modèle Dto associé au service
 * @param <T> le Modèle Dao associé au service
 */
@Slf4j
public abstract class BasicService<Z extends BasicDto, T extends BasicDao> {
    /**
     * récupère toutes les entités
     * 
     * @return la liste d'entité que l'on souhaite récupérer sous forme de DTO
     */
    public Optional<List<Z>> selectAll() {
        try {
            log.debug("selection de toutes les entitées de " + getClass().getName());
            List<T> entities = getRepository().findAll();
            return Optional
                    .ofNullable(entities.stream().map((element) -> getConverter().ConvertFromDaoToDtoModel(element))
                            .collect(Collectors.toList()));
        } catch (Exception e) {
            log.error("erreur au niveau de la selection de toutes les entitées de " + getClass().getName()
                    + " exception levée : " + e.getMessage());
            throw new RequestExceptions.Exception500();
        }
    }

    /**
     * poste une entité
     * 
     * @param dto le body que l'utilisateur souhaite ajouter
     * @return l'élément ajouté par l'utilisateur
     */
    public Optional<Z> post(final Optional<Z> dto) {
        if (dto.isEmpty())
            throw new RequestExceptions.Exception400("missing arguments");
        try {
            getRepository().save(getConverter().ConvertFromDtoToDaoModel(dto.get()));
            return dto;
        } catch (Exception e) {
            log.error("erreur ajout de : " + getClass().getName()
                    + " exception levée : " + e.getMessage());
            throw new RequestExceptions.Exception500();
        }

    }

    protected boolean validString(String str) {
        return str != null && str.length() >= 1;
    }

    /**
     * récupère le convertisseur DTO <-> DAO associé
     * 
     * @return le convertisseur
     */
    protected abstract BasicConverter<Z, T> getConverter();

    /**
     * récupére le répertoire JPA associé
     * 
     * @return le répertoire JPA
     */
    protected abstract JpaRepository<T, Long> getRepository();
}
