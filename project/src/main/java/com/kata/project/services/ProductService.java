package com.kata.project.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.kata.project.commons.exceptions.RequestExceptions;
import com.kata.project.converters.BasicConverter;
import com.kata.project.converters.ProductConverter;
import com.kata.project.models.dao.ProductDao;
import com.kata.project.models.dto.ProductDto;
import com.kata.project.repositories.ProductRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProductService extends BasicService<ProductDto, ProductDao> {

    @Autowired
    ProductConverter productConverter;

    @Autowired
    ProductRepository productRepository;

    @Override
    public Optional<ProductDto> post(Optional<ProductDto> dto) {
        if (dto.isEmpty()) {
            throw new RequestExceptions.Exception400("Null Offer");
        }
        ProductDto productDto = dto.get();
        if (!validString(productDto.getProductBrand()) || !validString(productDto.getProductSize())
                || !validString(productDto.getProductName()) ||
                productDto.getPrice() == null || productDto.getPrice() < 0 || 
                productDto.getQuantity() == null || productDto.getQuantity() <= 0) {
            log.debug("body invalide");
            throw new RequestExceptions.Exception400("one of arguments of the offer is invalid");
        }
        return super.post(dto);
    }

    /**
     * modifie une offre
     * 
     * @param dto les valeurs modifiées de l'offre
     * @return l'offre modifiée
     */
    public Optional<ProductDto> update(final Optional<ProductDto> dto) {
        if (dto.isEmpty() || dto.get().getProductId() == null) {
            throw new RequestExceptions.Exception400();
        }
        Optional<ProductDao> dataInDb = productRepository.findById(dto.get().getProductId());
        log.debug("offre a modifié existe en bdd");
        if (dataInDb.isPresent()) {
            ProductDao productToSet = dataInDb.get();
            productToSet.setProductBrand(dto.get().getProductBrand());
            productToSet.setProductSize(dto.get().getProductSize());
            productToSet.setProductName(dto.get().getProductName());
            productToSet.setPrice(dto.get().getPrice());
            productToSet.setQuantity(dto.get().getQuantity());
            try {
                productRepository.save(productToSet);
                log.debug("l'offre a bien été modifiée");
            } catch (Exception e) {
                log.error("l'offre n'a pas pu etre modifiée", e);
                throw new RequestExceptions.Exception500();
            }
        } else {
            log.debug("l'offre n'a pas pu étre modifiée car elle n'a pas été retrouvée en bdd");
            throw new RequestExceptions.Exception404("offer not found");
        }

        return dto;
    }

    

    @Override
    protected BasicConverter<ProductDto, ProductDao> getConverter() {
        log.debug("récupération du convertisseur");
        return productConverter;
    }

    @Override
    protected JpaRepository<ProductDao, Long> getRepository() {
        return productRepository;
    }
}
