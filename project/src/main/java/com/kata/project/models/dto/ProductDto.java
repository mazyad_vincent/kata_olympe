package com.kata.project.models.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto implements BasicDto {
    @ApiModelProperty(notes = "base de donnée auto generée id")
    private Long ProductId;
    @ApiModelProperty(notes = "le type de produit (ex : T shirt)")
    private String ProductName;
    @ApiModelProperty(notes = "Le nom de la marque")
    private String ProductBrand;
    @ApiModelProperty(notes = "la taille du produit (ex : M, S, XS ...)")
    private String ProductSize;
    @ApiModelProperty(notes = "la quantité de produit à acheter")
    private Integer Quantity;
    @ApiModelProperty(notes = "le prix")
    private Float Price;

}
