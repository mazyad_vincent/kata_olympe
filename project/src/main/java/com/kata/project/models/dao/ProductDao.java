package com.kata.project.models.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "offers")
public class ProductDao implements BasicDao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ProductId;
    @Column(name = "name")
    private String ProductName;
    @Column(name = "brand")
    private String ProductBrand;
    @Column(name = "size")
    private String ProductSize;
    @Column(name = "quantity")
    private Integer Quantity;
    @Column(name = "price")
    private Float Price;
}
