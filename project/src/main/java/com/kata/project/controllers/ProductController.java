package com.kata.project.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kata.project.commons.exceptions.ApiExceptionModel;
import com.kata.project.models.dto.ProductDto;
import com.kata.project.services.ProductService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

/**
 * Rest Controller
 * Path vers les opérations sur les offres
 */
@Slf4j
@RestController
@RequestMapping("/api/offer")
public class ProductController {

    @Autowired
    ProductService productService;

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Succès", response = ProductDto.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Paramètres invalides, devEui manquant", response = ApiExceptionModel.class),
            @ApiResponse(code = 404, message = "DevEui non trouvé", response = ApiExceptionModel.class),
            @ApiResponse(code = 500, message = "Erreur interne du serveur", response = ApiExceptionModel.class)
    })
    @ApiOperation(value = "récupère toutes les offres", produces = "application/json")
    @GetMapping(produces = "application/json", value = "/all")
    public ResponseEntity<List<ProductDto>> getAll() {
        log.info("récupération des offres");
        return ResponseEntity.status(HttpStatus.OK).body(productService.selectAll().get());
    }

    /**
     * ajoute une offre
     * 
     * @return l'offre ajoutée par l'utilisateur
     */
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Succès", response = ProductDto.class),
            @ApiResponse(code = 400, message = "Paramètres invalides", response = ApiExceptionModel.class),
            @ApiResponse(code = 500, message = "Erreur interne", response = ApiExceptionModel.class)
    })
    @ApiOperation(value = "ajoute une offre", produces = "application/json", consumes = "application/json")
    @PostMapping(produces = "application/json", value = "/add")
    public ResponseEntity<ProductDto> postOffer(@RequestBody ProductDto offer) {
        log.info("ajout d'une offre");
        return ResponseEntity.status(HttpStatus.CREATED).body(productService.post(Optional.ofNullable(offer)).get());
    }

    /**
     * met à jour une offre
     *
     * @param
     * @return l'entité modifiée
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Succès", response = ProductDto.class),
            @ApiResponse(code = 400, message = "Paramètres invalides", response = ApiExceptionModel.class),
            @ApiResponse(code = 404, message = "Offre non trouvée", response = ApiExceptionModel.class),
            @ApiResponse(code = 500, message = "Erreur interne", response = ApiExceptionModel.class)
    })
    @ApiOperation(value = "met à jour les données d'une offre", produces = "application/json")
    @PutMapping(produces = "application/json", value = "update")
    public ResponseEntity<ProductDto> update(@RequestBody ProductDto offer) {
        log.info("modification d'une offre");
        return ResponseEntity.status(HttpStatus.OK).body(productService.update(Optional.ofNullable(offer)).get());
    }
}
