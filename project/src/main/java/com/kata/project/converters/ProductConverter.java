package com.kata.project.converters;

import org.springframework.stereotype.Component;

import com.kata.project.models.dao.ProductDao;
import com.kata.project.models.dto.ProductDto;

import lombok.NonNull;

@Component
public class ProductConverter implements BasicConverter<ProductDto, ProductDao> {

    @Override
    public ProductDto ConvertFromDaoToDtoModel(@NonNull ProductDao productDao) {
        return new ProductDto(productDao.getProductId(),  productDao.getProductName(), productDao.getProductBrand(),
                productDao.getProductSize(), productDao.getQuantity(),
                productDao.getPrice());
    }

    @Override
    public ProductDao ConvertFromDtoToDaoModel(@NonNull ProductDto productDto) {
        return new ProductDao(productDto.getProductId(), productDto.getProductName(), productDto.getProductBrand(), productDto.getProductSize(),
                productDto.getQuantity(), productDto.getPrice());
    }

}
