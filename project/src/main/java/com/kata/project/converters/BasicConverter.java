package com.kata.project.converters;

import com.kata.project.models.dao.BasicDao;
import com.kata.project.models.dto.BasicDto;

import lombok.NonNull;

/**
 * convertis un dao en dto et inversement
 */
public interface BasicConverter<Z extends BasicDto, Y extends BasicDao> {
    /**
     * convertis un Dao en Dto (objet bdd en object client)
     * @param Dao le Dao à convertir 
     * @return le DTO convertis depuis le DAO
     */
    Z ConvertFromDaoToDtoModel(@NonNull final Y Dao);

    /**
     * convertis un DTO en DAO  
     * @param Dto le Dto à convertir 
     * @return le DAO convertis depuis le Dto
     */
    Y ConvertFromDtoToDaoModel(@NonNull final Z Dto);
}
