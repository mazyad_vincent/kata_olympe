package com.kata.project.repositories;
import org.springframework.data.jpa.repository.JpaRepository;

import com.kata.project.models.dao.ProductDao;

public interface ProductRepository extends JpaRepository<ProductDao, Long>{

}
