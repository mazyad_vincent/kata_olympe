Projet olympe réalisé par Vincent Mazyad
Version swagger utilisé 2.7.9, Java 17

H2 bdd utilisée
La bdd est alimentée par un script sql data.sql au moment de l'execution 

pour accèder au swagger :  http://localhost:8080/swagger-ui.html
pour accéder à la console h2 : http://localhost:8080/h2-console
JDBC URL doit etre : <jdbc:h2:mem:testdb>  
UserName : <sa>
passord : pas de mdp